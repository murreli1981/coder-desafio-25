const { Schema, model } = require("mongoose");

const messageSchema = new Schema({
  id: String,
  author: {
    id: String,
    nombre: String,
    apellido: String,
    edad: Number,
    alias: String,
    avatar: String,
  },
  date: String,
  message: String,
});

module.exports = model("Message", messageSchema);

// author: {
//   id: emailTxt.value,
//   nombre: nameTxt.value,
//   apellido: lastNameTxt.value,
//   edad: ageNum.value,
//   alias: aliasTxt.value,
//   avatar: avatarUrl.value,
// },
