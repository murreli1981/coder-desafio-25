require("dotenv").config();

module.exports = {
  PORT: process.env.PORT || 8080,
  MONGO_URI: process.env.MONGO_URI || "",
  SECRET_KEY: process.env.SECRET_KEY || "Qw3rt-yu10p01uyt-r3wQ",
};
